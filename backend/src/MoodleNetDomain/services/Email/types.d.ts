export type EmailObj = {
  to: string
  from: string
  subject: string
  text?: string
  html?: string
}
