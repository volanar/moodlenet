import { StoreSentEmailPersistence } from '../apis/Email.SendOne.Req'

interface EmailPersistence {
  storeSentEmail: StoreSentEmailPersistence
}
