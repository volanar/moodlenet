import { FC } from 'react';

export const MainPageWrapper: FC = ({ children }) => {
  return <div>{children}</div>;
};
